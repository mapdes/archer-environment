#! /bin/bash

module load cmake
module swap PrgEnv-cray PrgEnv-gnu
#module swap PrgEnv-cray PrgEnv-intel
module load cray-hdf5-parallel
module load cray-libsci

export CC=cc
export CXX=CC
export INTEL_PATH=/opt/intel/composer_xe_2013_sp1.1.106

mkdir -p src && cd src
if [ ! -f gmsh-2.8.5-source.tgz ]; then
  wget http://www.geuz.org/gmsh/src/gmsh-2.8.5-source.tgz
  tar xzf http://www.geuz.org/gmsh/src/gmsh-2.8.5-source.tgz
  # Patch CMakeLists.txt to not require libguide for MKL
  sed -ie 's/MKL_LIBS_REQUIRED mkl_gf_lp64 iomp5 mkl_gnu_thread mkl_core guide pthread/MKL_LIBS_REQUIRED mkl_gf_lp64 iomp5 mkl_gnu_thread mkl_core pthread/' gmsh-2.8.5-source/CMakeLists.txt
fi
cd gmsh-2.8.5-source
mkdir -p $WORK/gmsh/2.8.5
mkdir -p build && cd build
cmake -DCMAKE_CXX_FLAGS=-dynamic -DCMAKE_LIBRARY_PATH="$INTEL_PATH/mkl;$INTEL_PATH/compiler" -DCMAKE_LIBRARY_ARCHITECTURE=intel64 -DCMAKE_INSTALL_PREFIX=$WORK/gmsh/2.8.5 ..
make -j 24
make install
