#! /bin/bash

set -eaux

module load cmake
module swap PrgEnv-cray PrgEnv-gnu
# Gmsh does not build with Intel
#module swap PrgEnv-cray PrgEnv-intel
module load cray-hdf5-parallel
module load cray-libsci

export CC=cc
export CXX=CC
export INTEL_PATH=/opt/intel/composer_xe_2015.2.164
version=2.9.3
root=$FDRAKE_DIR/gmsh/$version
log=$root/build.log

mkdir -p $root/src && cd $root/src
if [ ! -d gmsh-$version-source ]; then
  wget -N http://www.geuz.org/gmsh/src/gmsh-$version-source.tgz
  tar xzf gmsh-$version-source.tgz
  # Patch CMakeLists.txt to not require libguide for MKL
  sed -ie 's/MKL_LIBS_REQUIRED mkl_gf_lp64 iomp5 mkl_gnu_thread mkl_core guide pthread/MKL_LIBS_REQUIRED mkl_gf_lp64 iomp5 mkl_gnu_thread mkl_core pthread/' gmsh-$version-source/CMakeLists.txt
fi
cd gmsh-$version-source
mkdir -p $root/build && cd $root/build
# CMAKE_LIBRARY_PATH needs to be set for MKL to be found
# Do NOT set CMAKE_PREFIX_PATH since we don't want includes to be picked up
# which are only for use with Intel compilers
# CMAKE_INSTALL_RPATH_USE_LINK_PATH needs to be set so the library directories
# linked to are kept in the install RPATH (by default they are stripped)
cmake -DCMAKE_CXX_FLAGS=-dynamic \
      -DCMAKE_LIBRARY_PATH="$INTEL_PATH/mkl/lib/intel64;$INTEL_PATH/compiler/lib/intel64" \
      -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE \
      -DCMAKE_INSTALL_PREFIX=$FDRAKE_DIR \
      $root/src/gmsh-$version-source | tee $log 2>&1
make -j 12 | tee -a $log 2>&1
make install | tee -a $log 2>&1
