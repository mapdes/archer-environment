#!/usr/bin/python
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--known-bits-per-byte=8',
    '--known-level1-dcache-assoc=4',
    '--known-level1-dcache-linesize=64',
    '--known-level1-dcache-size=16384',
    '--known-memcmp-ok=1',
    '--known-mpi-c-double-complex=1',
    '--known-mpi-long-double=1',
    '--known-mpi-shared-libraries=0',
    '--known-sizeof-MPI_Comm=4',
    '--known-sizeof-MPI_Fint=4',
    '--known-sizeof-char=1',
    '--known-sizeof-double=8',
    '--known-sizeof-float=4',
    '--known-sizeof-int=4',
    '--known-sizeof-long-long=8',
    '--known-sizeof-long=8',
    '--known-sizeof-short=2',
    '--known-sizeof-size_t=8',
    '--known-sizeof-void-p=8',
    '--with-ar=ar',
    '--with-cc=cc',
    '--with-clib-autodetect=0',
    '--with-cxx=CC',
    '--with-cxxlib-autodetect=0',
    '--with-fc=ftn',
    '--with-fortran-datatypes=0',
    '--with-fortran-interfaces=0',
    '--with-fortranlib-autodetect=0',
    '--with-ranlib=ranlib',
    '--with-scalar-type=real',
    '--with-shared-ld=ar',
    '--with-make-np=12',
    'COPTFLAGS=-hipa1 -hnopattern -O2 -hcpu=x86-64 -hpic ',
    'CXXOPTFLAGS=',
    'FOPTFLAGS=-F -em -hnocaf -hipa1 -hnopattern -O2 -hcpu=x86-64 -hpic ',
    '-O3',
    '-Wl,-Bdynamic',
    '-g',
    '--with-debugging=0',
    '--with-shared-libraries=1',
    '--download-triangle',
    '--download-ctetgen',
    '--download-chaco',
    '--download-hypre',
    'PETSC_ARCH=cray-gnu-shared',
  ]
  configure_options.append("--prefix="+os.getenv("FDRAKE_DIR"))
  configure.petsc_configure(configure_options)
