#! /bin/bash

version=${1:-0.22.1}

mkdir -p $FDRAKE_DIR/cython
cd $FDRAKE_DIR/cython
if [ ! -d Cython-$version ]; then
  wget -N http://cython.org/release/Cython-$version.tar.gz
  tar xzf Cython-$version.tar.gz 
fi
cd Cython-$version
python setup.py install --prefix=$FDRAKE_DIR 
