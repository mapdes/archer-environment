import mpi4py
print "Found mpi4py:", mpi4py.__file__

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.rank
print "[", rank, "] MPI.COMM_WORLD.size", MPI.COMM_WORLD.size

import petsc4py
print "[", rank, "] Found petsc4py:", petsc4py.__file__
import sys
petsc4py.init(sys.argv)
from petsc4py import PETSc  # NOQA get flake8 to ignore unused import. 

print "[", rank, "] Testing petsc4py: Distributing a simple DMPlex object:"
plex = PETSc.DMPlex().createBoxMesh(2, comm=MPI.COMM_WORLD)
plex.distribute(overlap=1)
plex.view()

import pyop2
print "[", rank, "] Found pyop2:", pyop2.__file__

import firedrake
print "[", rank, "] Found firedrake", firedrake.__file__

from firedrake import *
import numpy as np

mesh = UnitSquareMesh(10, 10)
print "[", rank, "] Testing Firedrake: Build 10x10 mesh, the Plex is:"
mesh._plex.view()

print "[", rank, "] Testing Firedrake: Identity (x[0]) on UnitSquareMesh(10x10), CG3"
fs = FunctionSpace(mesh, "Lagrange", 3)
f = Function(fs)
out = Function(fs)
u = TrialFunction(fs)
v = TestFunction(fs)
a = u * v * dx

f.interpolate(Expression("x[0]"))
L = f * v * dx
solve(a == L, out)
print "[", rank, "] Testing Firedrake; error:", np.max(np.abs(out.dat.data - f.dat.data))
