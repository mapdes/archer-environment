#! /bin/bash
module swap PrgEnv-cray PrgEnv-gnu
module load swig

mkdir -p src && cd src
if [ ! -d graphviz-2.38.0 ]; then
  wget http://www.graphviz.org/pub/graphviz/stable/SOURCES/graphviz-2.38.0.tar.gz
  tar xzf graphviz-2.38.0.tar.gz
fi
cd graphviz-2.38.0
./configure --prefix=$WORK/graphviz/2.38.0
make -j24
make install
