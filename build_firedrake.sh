#! /bin/bash
set -eax

echo Starting fdrake installation at `date`

if [ -z ${FDRAKE_DIR+x} ]; then
    echo "FDRAKE_DIR is unset. Please set it to the install directory before continuing."
    exit 1
fi
    
echo "==== Install and set up modules ==="

sh install_modules.sh
export MODULEPATH=$FDRAKE_DIR/modules:$MODULEPATH
module load firedrake

echo "==== Build PETSc ==="

pushd petsc
PETSC_DIR=$PWD python ../reconfigure-cray-gnu-shared.py
make PETSC_DIR=$PWD PETSC_ARCH=cray-gnu-shared all
make PETSC_DIR=$PWD PETSC_ARCH=cray-gnu-shared install
popd

echo "==== Build Cython ==="

./build_cython.sh

echo "==== Build mpi4py ==="

pushd mpi4py
python setup.py build --mpi=../mpi.cfg
python setup.py install --prefix=$FDRAKE_DIR
popd

echo "==== Build petsc4py ==="

pushd petsc4py
MPICC=cc python setup.py install --prefix=$FDRAKE_DIR
popd

echo "==== Build SWIG ==="

pushd swig/3.0.6
sh build_swig.sh
popd

echo "==== Build gmsh ==="

pushd gmsh/2.9.3
sh build_gmsh.sh
popd

echo "==== Build Firedrake Python packages ==="

pip install --no-use-wheel --install-option="--prefix=$FIREDRAKE_DIR" cachetools

for package in fenics/instant ufl ffc fiat coffee PyOP2 firedrake; do
  pushd $package
  LDSHARED='cc -shared' python setup.py install --prefix=$FIREDRAKE_DIR
  popd
done

echo "Finished fdrake installation at" `date`
