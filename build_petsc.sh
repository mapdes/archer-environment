#! /bin/bash
set -x
set -e

module unload PrgEnv-cray
module load PrgEnv-gnu
module load cmake
# module load git
# module load firedrake
# if [[ ! -d $PETSC_DIR ]]; then
#   git clone https://bitbucket.org/petsc/petsc.git
# fi
# Archive the old build in case we want to go back to it later
# if [[ -d $PETSC_ARCH ]]; then
#   mv $PETSC_ARCH $PETSC_ARCH-`git describe`
# fi
# Get latest changes
# git pull
# Disable a Chaco BLAS check that otherwise causes configure to fail
# by commenting out lines 47 and 48 in config/PETSc/packages/Chaco.py
for arch in shared; do
  #module unload petsc
  module load petsc/$arch
  (
  cd $PETSC_DIR
  python reconfigure-$PETSC_ARCH.py
  make PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH all
  )
  (
  cd petsc4py
  make clean
  MPICC=cc python setup.py install --prefix=$PETSC_DIR/$PETSC_ARCH
  )
done
