#! /bin/bash

# Usage: build_fenics.sh <tag>

set -eaux

LOGFILE=`pwd`/`date +"%Y-%m-%dT%H%M%S"`_build_fenics.log
#DOLFIN_TAG=${1:-master}

# module unload PrgEnv-cray
# module load PrgEnv-gnu
module use $FDRAKE_DIR/modules
module load git
module load fenics/master

echo "==== Modules ====" | tee -a $LOGFILE 2>&1
module list | tee -a $LOGFILE 2>&1
echo "==== Environment ====" | tee -a $LOGFILE 2>&1
env | tee -a $LOGFILE 2>&1

FENICS_DIR=$FDRAKE_DIR/fenics
mkdir -p $FENICS_DIR/lib/python2.7/site-packages

pip install --install-option="--prefix=$FENICS_DIR" ply

for f in fiat ffc ufl instant; do
  echo "*** Installing ${f} ***" | tee -a $LOGFILE
  (
   cd $f
   python setup.py install --prefix $FENICS_DIR | tee -a $LOGFILE 2>&1
  )
done

echo "*** Installing DOLFIN ***" | tee -a $LOGFILE
# if [ ! -d $DOLFIN_TAG ]; then
#   if [ $DOLFIN_TAG == "master" ]; then
#     # Clone DOLFIN master
#     git clone https://bitbucket.org/fenics-project/dolfin.git ${DOLFIN_TAG}
#   else
#     # Clone DOLFIN at given DOLFIN_TAG (note the prefix dolfin-)
#     git clone -b dolfin-${DOLFIN_TAG} https://bitbucket.org/fenics-project/dolfin.git ${DOLFIN_TAG}
#   fi
# fi

# Build
(
 cd dolfin
 echo "** Generating scripts **" | tee -a $LOGFILE
 ./cmake/scripts/generate-form-files | tee -a $LOGFILE 2>&1
 ./cmake/scripts/generate-cmakefiles | tee -a $LOGFILE 2>&1
 ./cmake/scripts/generate-swig-docstrings | tee -a $LOGFILE 2>&1
 ./cmake/scripts/generate-swig-interface | tee -a $LOGFILE 2>&1
 if [ ! -d build.`git rev-parse --short HEAD` ];then
     if [ -d build.master ]; then 
         mv build.master build.`git rev-parse --short HEAD`
     fi
 fi
 mkdir -p build.master && cd build.master
 echo "** Configuring **" | tee -a $LOGFILE
 cmake -DCMAKE_CXX_FLAGS=-std=c++11 \
   -DDOLFIN_SKIP_BUILD_TESTS=true -DDOLFIN_AUTO_DETECT_MPI=false \
   -DCMAKE_INSTALL_PREFIX=$FENICS_DIR -DUFC_DIR=$FENICS_DIR/share/ufc \
   -DBOOST_ROOT=$BOOST_DIR \
   -DEIGEN3_INCLUDE_DIR=$EIGEN_DIR/eigen3 \
   -DDOLFIN_ENABLE_QT=OFF -DDOLFIN_ENABLE_UNIT_TESTS=OFF \
   -DDOLFIN_ENABLE_VTK=OFF .. | tee -a $LOGFILE 2>&1
 echo "** Building **" | tee -a $LOGFILE
 make -j 24 | tee -a $LOGFILE 2>&1 && make install | tee -a $LOGFILE 2>&1
)
