# fdrake environment on ARCHER

## Setup notes.

0.  Clone the archer-environment repository and its submodules:

        git clone git@bitbucket.org:mapdes/archer-environment.git
        cd archer-environment
        git submodule init
        git submodule update

1.  Ensure `WORK` is set to your work directory, and set `FDRAKE_DIR`
    to the installation location.

2.  Install Firedrake by running `build_firedrake.sh` and continue with
    step 10 to install FEniCS, or follow the steps below.

3.  Install the modules and add the `modules` subdirectory of
    `archer-environment` to `MODULEPATH`:

        sh install_modules.sh
        export MODULEPATH=$FDRAKE_DIR/modules:$MODULEPATH
        module load firedrake

4.  Build PETSc:

        cd petsc
        PETSC_DIR=$PWD python ../reconfigure-cray-gnu-shared.py
        make PETSC_DIR=$PWD PETSC_ARCH=cray-gnu-shared all
        make PETSC_DIR=$PWD PETSC_ARCH=cray-gnu-shared install
        cd ..

5.  Build Cython:

        ./build_cython.sh

6.  Build mpi4py:

        cd mpi4py
        python setup.py build --mpi=../mpi.cfg
        python setup.py install --prefix=$FDRAKE_DIR
        cd ..

7.  Build petsc4py:

        cd petsc4py
        MPICC=cc python setup.py install --prefix=$FDRAKE_DIR
        cd ..

8.  Build swig:

        cd swig/3.0.6
        sh build_swig.sh
        cd ../..

9.  Build gmsh:

        cd gmsh/2.9.3
        sh build_gmsh.sh
        cd ../..

10. Install cachetools

        pip install --no-use-wheel --install-option="--prefix=$FIREDRAKE_DIR" cachetools

11. Build the python packages:

        for package in fenics/instant ufl ffc fiat coffee PyOP2 firedrake; do
            cd $package;
            python setup.py install --prefix=$FIREDRAKE_DIR;
            cd ..;
        done

12. Build DOLFIN

       cd fenics
       sh build_fenics.sh
       cd ..

## Original setup notes (outdated)

1.  All modules are installed in `/work/y07/y07/fdrake` with the Firedrake build environement:

        cd /work/y07/y07/fdrake
        module swap PrgEnv-cray fdrake-build-env

2 . Build mpi4py with cray-mpich (custom settings defined in mpi.cfg)

        python setup.py build --mpi=cray-mpich-gnu
        python setup.py install --prefix=/work/y07/y07/fdrake/mpi4py

3.  Build PETSc (`PETSC_DIR` and `PETSC_ARCH` are currently set in build-env)

        git clone https://bitbucket.org/petsc/petsc.git
        cd $PETSC_DIR

    Disable a Chaco BLAS check that otherwise causes configure to fail
    by commenting out lines 47 and 48 in `config/PETSc/packages/Chaco.py`

        python reconfigure-$PETSC_ARCH.py
        make PETSC_DIR=/work/y07/y07/fdrake/petsc PETSC_ARCH=cray-gnu-fdrake all

4.  Build petsc4py (can't use pip due to NumPy linking error)

        git clone https://bitbucket.org/petsc/petsc4py.git
        cd petsc4py
        MPICC=cc python setup.py install --prefix=/work/y07/y07/fdrake/petsc4py   

5.  Build dependencies

        MPICC=cc pip install git+https://bitbucket.org/mapdes/ufl.git#egg=ufl --ignore-installed --install-option="--prefix=/work/y07/y07/fdrake/ufl"
        MPICC=cc pip install git+https://bitbucket.org/mapdes/fiat.git#egg=fiat --ignore-installed --install-option="--prefix=/work/y07/y07/fdrake/fiat"
        MPICC=cc pip install git+https://bitbucket.org/fenics-project/instant.git#egg=instant --ignore-installed --install-option="--prefix=/work/y07/y07/fdrake/instant"
        MPICC=cc pip install psutil --ignore-installed --install-option="--prefix=/work/y07/y07/fdrake/psutil"

        hg clone https://bitbucket.org/khinsen/scientificpython
        MPICC=cc python setup.py install --prefix=/work/y07/y07/fdrake/scientificpython

    The following need to be installed with module swap PrgEnv-cray firedrake,
    because they require the right PYTHONPATH

        git clone https://github.com/OP2/PyOP2.git
        MPICC=cc python setup.py install --prefix=/work/y07/y07/fdrake/PyOP2

    Firedrake calls `pyop2.get_petsc_dir` during setup, which result in MPI
    erros in the front-end. Instead get `PETSC_DIR` and `PETSC_ARCH` from env
    in `setup.py`

        git clone https://github.com/firedrakeproject/firedrake.git
        MPICC=cc python setup.py install --prefix=/work/y07/y07/fdrake/firedrake

    Install SWIG, required by FFC

        wget http://sourceforge.net/projects/swig/files/latest/download 
        tar -xvf swig-3.0.0.tar.gz
        ./configure --without-pcre --prefix=/work/y07/y07/fdrake/swig-3.0.0
        make && make install

        git clone https://bitbucket.org/mapdes/ffc
        MPICC=cc python setup.py install --prefix=/work/y07/y07/fdrake/ffc
