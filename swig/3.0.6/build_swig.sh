#! /bin/bash

mkdir -p src && cd src
wget http://prdownloads.sourceforge.net/swig/swig-3.0.6.tar.gz
tar xzf swig-3.0.6.tar.gz 
cd swig-3.0.6
./configure --without-pcre --prefix=$FDRAKE_DIR
make -j12
make install
